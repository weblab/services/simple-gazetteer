/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.gazetteer.api;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;

public abstract class TextAnnotator implements Analyser {


	protected Log logger;


	public TextAnnotator(final Log logger) {
		super();
		this.logger = logger;
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException, InvalidParameterException,
			ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		final List<Text> texts = this.checkParameters(args);
		this.logger.debug("Start processing resource " + args.getResource() + ". It contains " + texts.size() + " texts.");

		for (final Text text : texts) {
			this.logger.info("Processing text.");
			this.process(text);
		}

		final ProcessReturn ret = new ProcessReturn();
		ret.setResource(args.getResource());
		return ret;
	}


	/**
	 * Create an annotation on a text based on element detected.
	 *
	 * @param text
	 *            the Text unit being annotated
	 * @param offset
	 *            the offset of the detected element
	 * @param word
	 *            the word being detected
	 * @param className
	 *            the class name of the detected object
	 * @param label
	 *            the canonical label to use
	 * @param wpa
	 *            The WebLab processing annotator
	 */
	public void annote(final Text text, final int offset, final String word, final String className, final String label, final WProcessingAnnotator wpa) {
		this.logger.debug("Creating segment : " + offset + " - " + word);
		final LinearSegment linearSegment = SegmentFactory.createAndLinkLinearSegment(text, offset, offset + word.length());

		final URI instanceUri = URI.create(WebLabProcessing.NAMESPACE + "temp-" + UUID.randomUUID().toString());
		wpa.startInnerAnnotatorOn(URI.create(linearSegment.getUri()));
		wpa.writeRefersTo(instanceUri);
		wpa.endInnerAnnotator();
		wpa.startInnerAnnotatorOn(instanceUri);
		wpa.writeType(URI.create(WebLabProcessing.NAMESPACE + "demo-" + className));
		wpa.writeLabel(label);
		wpa.writeCandidate(Boolean.TRUE);
		wpa.endInnerAnnotator();

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Word annotated: " + word + " class " + className + " -> " + offset);
			this.logger.debug("In text: '" + text.getContent().substring(offset, offset + word.length()) + "'");
		}

	}


	public abstract void process(Text text) throws AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException, InvalidParameterException, ServiceNotConfiguredException,
			UnexpectedException, UnsupportedRequestException;


	/**
	 * @param args
	 *            The ProcessArgs
	 * @return The list of Text contained by the Resource in args.
	 * @throws InvalidParameterException
	 *             For any reason preventing the retrieval of text unit to be done.
	 */
	protected List<Text> checkParameters(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "ProcessArgs was null.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		final Resource res = args.getResource();
		if (res == null) {
			final String message = "Resource in ProcessArg was null.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof MediaUnit)) {
			final String message = "This service only process MediaUnit; Resource was a: " + res.getClass().getSimpleName() + ".";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		final List<Text> texts;
		if (res instanceof Text) {
			texts = new LinkedList<>();
			texts.add((Text) res);
		} else {
			texts = ResourceUtil.getSelectedSubResources(args.getResource(), Text.class);
		}

		// Removing empty text to prevent useless processing
		for (final ListIterator<Text> textIt = texts.listIterator(); textIt.hasNext();) {
			final Text text = textIt.next();
			if ((text.getContent() == null) || text.getContent().replaceAll("\\s+", "").isEmpty()) {
				textIt.remove();
			}
		}

		return texts;
	}



}
