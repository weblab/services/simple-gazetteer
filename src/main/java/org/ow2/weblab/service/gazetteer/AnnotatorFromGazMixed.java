/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.gazetteer;

import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.service.gazetteer.api.TextAnnotator;

public class AnnotatorFromGazMixed extends TextAnnotator {


	public AnnotatorFromGazMixed(final Log logger) {
		super(logger);
	}


	@Override
	public void process(final Text text) {

		final String content = text.getContent().toLowerCase();


		final WProcessingAnnotator wpa = new WProcessingAnnotator(text);

		/*
		 * for each Gaz class
		 */
		for (final Entry<String, Set<String>> gazEntry : GazetteerImpl.gaz.entrySet()) {
			this.logger.debug("Entering gaz class : " + gazEntry.getKey() + " - " + gazEntry.getValue().size());

			/*
			 * for each Gaz
			 */
			for (final String word : gazEntry.getValue()) {
				int index = 0;
				// int indexNorm = 0;
				while ((index = content.indexOf(word.toLowerCase(), index)) >= 0) {
					boolean before = true;
					boolean after = true;

					/*
					 * not the first letter
					 */
					if (index > 0) {
						before = ("" + content.charAt(index - 1)).matches("\\W");
					}

					/*
					 * not the last one
					 */
					if ((index + word.length()) < content.length()) {
						after = ("" + content.charAt(index + word.length())).matches("\\W");
					}

					if (before && after) {
						this.annote(text, index, word, gazEntry.getKey(), word, wpa);
					}
					index += word.length();
				}
			}
		}

	}
}
