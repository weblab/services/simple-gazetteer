/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.gazetteer;

import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.service.gazetteer.api.TextAnnotator;

public class AnnotatorFromGazFullRegexp extends TextAnnotator {


	public AnnotatorFromGazFullRegexp(final Log logger) {
		super(logger);
	}


	@Override
	public void process(final Text text) {
		final String content = ASCIINormaliser.normalise(text.getContent());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(text);

		/*
		 * for each Gaz class
		 */
		for (final Entry<String, Set<String>> gazEntry : GazetteerImpl.gaz.entrySet()) {
			this.logger.debug("Entering gaz class : " + gazEntry.getKey() + " - " + gazEntry.getValue().size());
			/*
			 * for each Gaz
			 */
			for (final String word : gazEntry.getValue()) {
				final String normalisedWord = ASCIINormaliser.normalise(word);
				this.logger.trace("Test gaz word : '" + normalisedWord + "' - " + normalisedWord.length());
				if (normalisedWord.length() > 0) {
					final Pattern p = Pattern.compile("(?:^|\\W)(" + normalisedWord + ")(?:$|\\W)");
					final Matcher m = p.matcher(content);
					/*
					 * for each matched word in the text
					 */
					while (m.find()) {
						this.annote(text, m.start(1), normalisedWord, gazEntry.getKey(), word, wpa);
					}
				}
			}
		}

	}
}
