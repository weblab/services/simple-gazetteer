/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.gazetteer;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;

@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class GazetteerImpl implements Analyser {


	@Resource
	protected WebServiceContext webServiceContext;


	protected static Map<String, Set<String>> gaz;


	protected Analyser annotator;


	protected Log logger;


	protected static boolean LOADED = false;


	/*
	 * before first call
	 */
	@PostConstruct
	public void init() {

		this.logger = LogFactory.getLog(this.getClass());
		// this.annotator = new AnnotatorFromGazFullRegexp(this.logger);
		this.annotator = new AnnotatorFromGazMixed(this.logger);
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException,
			InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException {
		if (!GazetteerImpl.LOADED) {
			final ServletContext servletContext = (ServletContext) this.webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
			this.loadGaz(new File(servletContext.getRealPath("WEB-INF/classes/gaz/")), ".lst");
		}
		return this.annotator.process(args);
	}


	protected synchronized void loadGaz(final File gazFolder, final String suffix) {
		final SuffixFilefilter suffixFilefilter = new SuffixFilefilter(suffix);
		GazetteerImpl.gaz = Collections.synchronizedMap(new HashMap<String, Set<String>>());
		for (final File file : gazFolder.listFiles(suffixFilefilter)) {
			try {
				this.loadFile(file);
			} catch (final IOException e) {
				this.logger.error("Unable to load Gazetteer file : " + file.getAbsolutePath(), e);
			}
		}
		this.logger.debug("Gazetteer loaded: " + GazetteerImpl.gaz);
		GazetteerImpl.LOADED = true;
	}


	/*
	 * load lst file in the service gaz Set.
	 */
	protected void loadFile(final File gazFile) throws IOException {
		final String className = gazFile.getName().split("\\.")[0];
		final Set<String> wordSet = new HashSet<>();
		if (GazetteerImpl.gaz.get(className) != null) {
			throw new IOException("Gazetteer class '" + className + "' is already set.");
		}

		final LineIterator iterator = FileUtils.lineIterator(gazFile, "UTF-8");
		try {
			while (iterator.hasNext()) {
				String word = iterator.nextLine().trim();
				if (!word.isEmpty()) {
					wordSet.add(word);
				}
			}
		} finally {
			LineIterator.closeQuietly(iterator);
		}

		GazetteerImpl.gaz.put(className, wordSet);
		this.logger.info("Loading finish for class '" + className + "', " + wordSet.size() + " words loaded.");
	}
}
