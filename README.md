# Simple Gazetteer service

This project is a Maven based Java project of a Web Service.
This WebLab WebService is more like a sample than a real project. It aims at extracting named entities based on gazetteers (dictionaries). In case you really whant to extract named entities you'd better have a look at the [gate-extraction service](https://gitlab.ow2.org/weblab/services/gate-extraction).
It has also a few other interfaces (analyser, searcher) that could be used.


# Build status

## Master
[![build status](https://gitlab.ow2.org/weblab/services/simple-gazetteer/badges/master/build.svg)](https://gitlab.ow2.org/weblab/services/simple-gazetteer/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/services/simple-gazetteer/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/services/simple-gazetteer/commits/develop)


# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
